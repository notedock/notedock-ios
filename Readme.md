# NoteDock

NoteDock is a multiplatform application for saving simple notes and organizing them into folders. They are automatically synchronized across all platforms in real time.

## Platforms

 * Android: App got pulled from Play Store and source code can be found [here](https://gitlab.com/notedock/notedock-android).
 * iOS: App created but refused to be accepted into app store and source code can be found [here](https://gitlab.com/notedock/notedock-ios).
 * Web: [NoteDock](https://notedock.firebaseapp.com/). Web page was created by [Martin Pilát](https://www.linkedin.com/in/martin-pil%C3%A1t-ab319021b/).

