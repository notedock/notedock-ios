//
//  ResetPasswordViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseAuthentication
import Crashlytics

protocol ResetPasswordViewViewModelProtocol {
    var resetPassword: PublishSubject<ResetPasswordEvent> {get}
    var showLoading: PublishSubject<Bool> {get}
    func resetPassword(email: String)
}

class ResetPasswordViewViewModel: ResetPasswordViewViewModelProtocol {
    var resetPassword: PublishSubject<ResetPasswordEvent> = PublishSubject()
    var showLoading: PublishSubject<Bool> = PublishSubject()
    
    var authRepository: AuthRepository!
    var emailValidator: EmailValidatorUseCase!
    
    func resetPassword(email: String) {
        if (!emailValidator.validateEmail(email: email)) {
            self.resetPassword.onNext(.Error(.InvalidEmail))
        } else {
            self.showLoading.onNext(true)
            authRepository
                .sendPasswordResetEmail(email: email)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onError: { (error) in
                    self.showLoading.onNext(false)
                    self.resetPassword.onNext(.Error(self.handleError(error: error as NSError)))
                }, onCompleted: {
                    self.showLoading.onNext(false)
                    self.resetPassword.onNext(.Success)
                })
        }
    }
    
    func handleError(error: NSError) -> ResetPasswordError {
        switch error.code {
            case AuthErrorCode.invalidEmail.rawValue: return .InvalidEmail
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.userNotFound.rawValue: return .NoAccount
            default: do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
}


enum ResetPasswordEvent {
    case Success
    case Error(ResetPasswordError)
}

enum ResetPasswordError {
    case NetworkError
    case NoAccount
    case InvalidEmail
    case UnknownError
}
