//
//  RegisterViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol RegisterViewViewModelProtocol {
    var register: PublishSubject<RegisterEvent> {get}
    var showLoading: PublishSubject<Bool> {get}
    func register(email: String, password: String, confirmPassword: String)
}

class RegisterViewViewModel: RegisterViewViewModelProtocol {
    var register: PublishSubject<RegisterEvent> = PublishSubject()
    var showLoading: PublishSubject<Bool> = PublishSubject()
    
    var emailValidator: EmailValidatorUseCase!
    var authRepository: AuthRepository!
    
    func register(email: String, password: String, confirmPassword: String) {
        if(!password.elementsEqual(confirmPassword)) {
            self.register.onNext(.Error(.PasswordDoNotMatch))
        } else if(!emailValidator.validateEmail(email: email)) {
                self.register.onNext(.Error(.InvadidEmail))
        } else {
            self.showLoading.onNext(true)
            authRepository
                .register(email: email, password: password)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onError: { error in
                    self.showLoading.onNext(false)
                    self.register.onNext(.Error(self.handleEmailError(error: error as NSError)))
                }, onCompleted: {
                    self.sendVerificationEmail()
                })
        }
    }
    
    func handleRegisterError(error: NSError) -> RegisterEventError  {
        switch error.code {
            case AuthErrorCode.invalidEmail.rawValue: return .InvadidEmail
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.weakPassword.rawValue: return .WeakPassword
            case AuthErrorCode.emailAlreadyInUse.rawValue: return .EmailAlreadyUsed
            default:do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
    
    private func sendVerificationEmail() {
        if let user = Auth.auth().currentUser {
            authRepository
            .sendVerificationEmail(user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onError: { error in
                self.showLoading.onNext(false)
                self.register.onNext(.Error(self.handleEmailError(error: error as NSError)))
            }, onCompleted: {
                self.showLoading.onNext(false)
                self.register.onNext(.Success)
            })
        }
    }
    
    private func handleEmailError(error: NSError) -> RegisterEventError {
        switch error.code {
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.tooManyRequests.rawValue: return .TooManyRequests
            default: do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
}

enum RegisterEvent {
    case Success
    case Error(RegisterEventError)
}

enum RegisterEventError {
    case EmailAlreadyUsed
    case PasswordDoNotMatch
    case NetworkError
    case InvadidEmail
    case WeakPassword
    case UnknownError
    case TooManyRequests
}
