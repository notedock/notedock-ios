//
//  LoginViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol LoginViewViewModelProtocol {
    var login: PublishSubject<LoginEvent> {get}
    var showLoading: PublishSubject<Bool> {get}
    var sendVerificationEmail: PublishSubject<EmailEvent> {get}
    func login(email: String, password: String)
    func sendVerificationEmail(user: User)
}

class LoginViewViewModel: LoginViewViewModelProtocol {

    var showLoading: PublishSubject<Bool> = PublishSubject()
    var login: PublishSubject<LoginEvent> = PublishSubject()
    var sendVerificationEmail: PublishSubject<EmailEvent> = PublishSubject()
    
    var emailValidator: EmailValidatorUseCase!
    
    var authRepository: AuthRepository!
    
    func login(email: String, password: String) {
        self.showLoading.onNext(true)
        if(!emailValidator.validateEmail(email: email)) {
            self.login.onNext(.Error(.InvalidEmail))
        } else {
            self.authRepository
            .login(email: email, password: password)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onError: { (error) in
                self.showLoading.onNext(false)
                self.login.onNext(.Error(self.handleError(error: error as NSError)))
            }, onCompleted: {
                self.showLoading.onNext(false)
                self.isUserVerified()
            })
        }
    }
    
    func sendVerificationEmail(user: User) {
        self.showLoading.onNext(true)
        self.authRepository
            .sendVerificationEmail(user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onError: { error in
                self.showLoading.onNext(false)
                self.sendVerificationEmail.onNext(.Error(self.handleEmailError(error: error as NSError)))
            }, onCompleted: {
                self.showLoading.onNext(false)
                self.sendVerificationEmail.onNext(.Success)
            })
    }
    
    private func handleError(error: NSError) -> LoginError {
        switch error.code {
        case AuthErrorCode.invalidEmail.rawValue: return .InvalidEmail
        case AuthErrorCode.networkError.rawValue: return .NetworkError
        case AuthErrorCode.wrongPassword.rawValue: return .BadCredentials
        case AuthErrorCode.userNotFound.rawValue: return .BadCredentials
            default:do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
    
    private func handleEmailError(error: NSError) -> EmailEventError {
        switch error.code {
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.tooManyRequests.rawValue: return .TooManyRequests
            default: do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
    
    private func isUserVerified() {
        if let user = Auth.auth().currentUser {
            if(user.isEmailVerified == true) {
                self.login.onNext(.Success)
            } else {
                self.login.onNext(.Error(.UserNotVerified))
            }
        }
    }
}

enum EmailEvent {
    case Success
    case Error(EmailEventError)
}

enum EmailEventError {
    case NetworkError
    case TooManyRequests
    case UnknownError
}

enum LoginError {
    case InvalidEmail
    case BadCredentials
    case NetworkError
    case UserNotVerified
    case UnknownError
}

enum LoginEvent {
    case Success
    case Error(LoginError)
}
