//
//  NoteViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol NoteViewViewModelProtocol {
    var noteLoaded: PublishSubject<LoadNoteEvent> {get}
    var noteDeleted: PublishSubject<DeleteNoteEvent> {get}
    var noteSaved: PublishSubject<SaveNoteEvent> {get}
    func loadNote(folderUUID: String, noteUUID: String)
    func deleteNote(folderUUID: String, noteUUID: String)
    func updateNote(folderUUID: String, note: NoteModel)
}

class NoteViewViewModel: NoteViewViewModelProtocol {
    var noteLoaded: PublishSubject<LoadNoteEvent> = PublishSubject()
    var noteDeleted: PublishSubject<DeleteNoteEvent> = PublishSubject()
    var noteSaved: PublishSubject<SaveNoteEvent> = PublishSubject()
    var loadNoteFromFirebase: LoadNoteFromFirebaseUseCase?
    var deleteNoteFromFirebase: DeleteNoteFromFirebaseUseCase?
    var updateNoteFromFirebase: UpdateNoteToFirebaseUsecase?
    
    func loadNote(folderUUID: String, noteUUID: String) {
        if let user = Auth.auth().currentUser {
            loadNoteFromFirebase?
                .loadNote(folderUUID: folderUUID, noteUUID: noteUUID, user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { note in
                    self.noteLoaded.onNext(.Success(note))
                }, onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.noteLoaded.onNext(.Error(.UnknownError))
                })
        } else {
            self.noteLoaded.onNext(.Error(.NoUserFound))
        }
    }
    
    func deleteNote(folderUUID: String, noteUUID: String) {
        if let user = Auth.auth().currentUser {
            deleteNoteFromFirebase?
                .deleteNote(folderUUID: folderUUID, noteUUID: noteUUID, user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.noteDeleted.onNext(.Error(.UnknownError))
                }, onCompleted: {
                    self.noteDeleted.onNext(.Success)
                })
        } else {
            self.noteDeleted.onNext(.Error(.NoUserFound))
        }
    }
    
    func updateNote(folderUUID: String, note: NoteModel) {
        if let user = Auth.auth().currentUser {
            updateNoteFromFirebase?
                .updateNote(folderUUID: folderUUID, note: note, user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.noteSaved.onNext(.Error(.UnknownError))
                }, onCompleted: {
                    self.noteSaved.onNext(.Success)
                })
        } else {
            self.noteSaved.onNext(.Error(.NoUserFound))
        }
    }
}

enum LoadNoteEvent {
    case Success(NoteModel)
    case Error(LoadNoteEventError)
}

enum LoadNoteEventError {
    case UnknownError
    case NoUserFound
}

enum DeleteNoteEvent {
    case Success
    case Error(DeleteNoteEventError)
}

enum DeleteNoteEventError {
    case UnknownError
    case NoUserFound
}

enum SaveNoteEvent {
    case Success
    case Error(SaveNoteEventError)
}

enum SaveNoteEventError {
    case UnknownError
    case NoUserFound
}
