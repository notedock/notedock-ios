//
//  AccountViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 01/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import Crashlytics

protocol AccountViewViewModelProcotol {
    var accountDeleted: PublishSubject<DeleteAccountEvent> {get}
    var updateDisplayName: PublishSubject<UpdateEvent> {get}
    var showLoading: PublishSubject<Bool> {get}
    func deleteAccount(password: String)
    func updateDisplayName(newName: String?)
}

class AccountViewViewModel: AccountViewViewModelProcotol {
    var accountDeleted: PublishSubject<DeleteAccountEvent> = PublishSubject()
    var updateDisplayName: PublishSubject<UpdateEvent> = PublishSubject()
    var showLoading: PublishSubject<Bool> = PublishSubject()
    var authenticationRepository: AuthRepository!
    
    func deleteAccount(password: String) {
        //First check if his password is alright
        if let user = Auth.auth().currentUser {
            if let email = user.email {
                showLoading.onNext(true)
                authenticationRepository
                    .reauthenticateUser(user: user, email: email, password: password)
                    .asObservable()
                    .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                    .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                    .subscribe(onNext: { correctPassword in
                        self.deleteAccountFromFirebase(user: user)
                    }, onError: { error in
                        self.showLoading.onNext(false)
                        self.accountDeleted.onNext(.Error(self.handleDeleteError(error: error as NSError)))
                    })
            } else {
                self.accountDeleted.onNext(.Error(.NoEmailFound))
            }
        } else {
            self.accountDeleted.onNext(.Error(.NoUserFound))
        }
    }
    
    private func deleteAccountFromFirebase(user: User) {
        authenticationRepository
            .deleteUser(user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { deleted in
                self.showLoading.onNext(false)
                self.accountDeleted.onNext(.Deleted)
            }, onError: { error in
                self.showLoading.onNext(false)
                self.accountDeleted.onNext(.Error(self.handleDeleteError(error: error as NSError)))
            })
    }
    
    func updateDisplayName(newName: String?) {
        self.showLoading.onNext(true)
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = newName
        changeRequest?.commitChanges { error in
            self.showLoading.onNext(false)
            if let error = error {
                Crashlytics.sharedInstance().recordError(error)
                self.updateDisplayName.onNext(.Error(.WhoKnows))
            } else {
                self.updateDisplayName.onNext(.Success)
            }
        }
    }
    
    func handleDeleteError(error: NSError) -> DeleteAccountError {
        switch error.code {
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.wrongPassword.rawValue: return .WrongPassword
            default: do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
}

enum UpdateEvent {
    case Success
    case Error(UpdateEventError)
}

enum UpdateEventError {
    case WhoKnows
}

enum DeleteAccountEvent {
    case Deleted
    case Error(DeleteAccountError)
}

enum DeleteAccountError {
    case NoUserFound
    case NetworkError
    case UnknownError
    case WrongPassword
    case NoEmailFound
}
