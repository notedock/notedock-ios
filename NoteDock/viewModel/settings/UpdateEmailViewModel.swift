//
//  UpdateEmailViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 21/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import FirebaseAuth
import RxSwift
import Crashlytics

protocol UpdateEmailViewModelProtocol {
    var showLoading: PublishSubject<Bool> {get}
    var updateEmail: PublishSubject<UpdateEmailEvent> {get}
    func updateEmail(email: String, password: String)
}

class UpdateEmailViewModel: UpdateEmailViewModelProtocol {
    
    var updateEmail: PublishSubject<UpdateEmailEvent> = PublishSubject()
    var showLoading: PublishSubject<Bool> = PublishSubject()
    
    var email: String!
    
    var authRepository: AuthRepository!
    var emailValidator: EmailValidatorUseCase!
    
    private func checkPassword(password: String) {
        //If theres no signed in user
        guard let user = Auth.auth().currentUser else {
            self.updateEmail.onNext(.Error(.NoUserFound))
            return
        }
        //If user does not have an email
        guard let userEmail = user.email else {
            self.updateEmail.onNext(.Error(.UnknownError))
            return
        }
        //If users enters current email
        if userEmail.elementsEqual(self.email) {
            self.updateEmail.onNext(.Error(.UseNewEmail))
            return
        }
        //If email is badly formatted
        if(!emailValidator.validateEmail(email: self.email)) {
            self.updateEmail.onNext(.Error(.InvalidEmail))
        } else {
            self.showLoading.onNext(true)
            authRepository
                .reauthenticateUser(user: user, email: userEmail, password: password)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { correctPassword in
                    self.updateEmailUser(user: user)
                }, onError: { error in
                    self.showLoading.onNext(false)
                    self.updateEmail.onNext(.Error(self.handleErrors(error: error as NSError)))
            })
        }
    }
    
    private func updateEmailUser(user: User) {
        authRepository
            .updateEmail(user: user, email: self.email)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { correctPassword in
                self.sendVerificationEmail(user: user)
                self.showLoading.onNext(false)
            }, onError: { error in
                self.showLoading.onNext(false)
                self.updateEmail.onNext(.Error(self.handleErrors(error: error as NSError)))
            })
    }
    
    private func handleErrors(error: NSError) -> UpdateEmailEventError {
        switch error.code {
            case AuthErrorCode.networkError.rawValue: return .NetworkError
            case AuthErrorCode.emailAlreadyInUse.rawValue: return .EmailAlreadyUsed
            case AuthErrorCode.wrongPassword.rawValue: return .InvalidPassword
            case AuthErrorCode.invalidEmail.rawValue: return .InvalidEmail
            //This happens if user wants to change email and cached FirebaseUser has old email address
            case AuthErrorCode.userMismatch.rawValue:do {
                Auth.auth().currentUser?.reload(completion: { error in
                    if let error = error {
                        Crashlytics.sharedInstance().recordError(error)
                    }
                })
                return .TryAgainError
            }
            default:do {
                Crashlytics.sharedInstance().recordError(error)
                return .UnknownError
            }
        }
    }
    
    private func sendVerificationEmail(user: User) {
        self.authRepository
        .sendVerificationEmail(user: user)
        .asObservable()
        .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        .subscribe(onNext: { correctPassword in
            self.updateEmail.onNext(.Success)
            self.showLoading.onNext(false)
        }, onError: { error in
            self.showLoading.onNext(false)
            self.updateEmail.onNext(.Error(self.handleErrors(error: error as NSError)))
        })
    }
    
    func updateEmail(email: String, password: String) {
        self.email = email
        self.checkPassword(password: password)
    }
}

enum UpdateEmailEvent {
    case Success
    case Error(UpdateEmailEventError)
}

enum UpdateEmailEventError {
    case NetworkError
    case UseNewEmail
    case UnknownError
    case NoUserFound
    case InvalidEmail
    case EmailAlreadyUsed
    case InvalidPassword
    case TryAgainError
}
