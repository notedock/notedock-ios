//
//  EditFolderViewViewModel.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebase
import Crashlytics

protocol EditFolderViewViewModelProtocol {
    var folderDeleted: PublishSubject<DeleteFolderEvent> {get}
    var folderRenamed: PublishSubject<RenameFolderEvent> {get}
    func deleteFolder(folderUUID: String)
    func renameFolder(folderUUID: String, newName: String)
}

class EditFolderViewViewModel: EditFolderViewViewModelProtocol {
    public var folderRenamed: PublishSubject<RenameFolderEvent> = PublishSubject()
    public var folderDeleted: PublishSubject<DeleteFolderEvent> = PublishSubject()
    var deleteFolder: DeleteFolderFromFirebaseUseCase!
    var renameFolder: RenameFolderUsecase!
    var folderNameTaken: FolderNameTakenUseCase!
    
    func deleteFolder(folderUUID: String) {
        if let user = Auth.auth().currentUser {
            self.deleteFolder
            .deleteFolder(folderUUID: folderUUID, user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onError: { error in
                Crashlytics.sharedInstance().recordError(error)
                self.folderDeleted.onNext(.Error(.UnknownError))
            }, onCompleted: {
                self.folderDeleted.onNext(.Success)
            })
        } else {
            self.folderDeleted.onNext(.Error(.NoUserFound))
        }
    }
    
    public func isNameTaken(folderUUID: String, folderName: String) {
        if let user = Auth.auth().currentUser {
            self.folderNameTaken
                .isNameTaken(folderName: folderName,user: user)
                .asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { nameTaken in
                    if(!nameTaken) {
                        self.renameFolder(folderUUID: folderUUID, newName: folderName, user: user)
                    } else {
                        self.folderRenamed.onNext(.Error(.NameTaken))
                    }
                }, onError: { error in
                    Crashlytics.sharedInstance().recordError(error)
                    self.folderRenamed.onNext(.Error(.UnknownError))
            })
        } else {
            self.folderDeleted.onNext(.Error(.NoUserFound))
        }
    }
    
    func renameFolder(folderUUID: String, newName: String) {
        self.isNameTaken(folderUUID: folderUUID, folderName: newName)
    }
    
    private func renameFolder(folderUUID: String, newName: String, user: User) {
        self.renameFolder
            .renameFolder(folderUUID: folderUUID, newName: newName, user: user)
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onError: { error in
                Crashlytics.sharedInstance().recordError(error)
                self.folderRenamed.onNext(.Error(.UnknownError))
            }, onCompleted: {
                self.folderRenamed.onNext(.Success)
            })
    }
}

enum RenameFolderEvent {
    case Success
    case Error(RenameFolderError)
}

enum RenameFolderError {
    case NameTaken
    case UnknownError
}
