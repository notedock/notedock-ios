//
//  EditFolderViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class EditFolderViewController: UIViewController {
    
    var folderUUID: String!
    var folderName: String!
    var editFolderViewModel: EditFolderViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    @IBOutlet var textFieldTitle: UITextField!
    
    @IBOutlet var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        self.title = folderUUID
        
        setupDataBinding()
        
        textFieldTitle.text = folderName
        //Empty textField disables Save Button
        textFieldTitle.addTarget(self, action: #selector(EditFolderViewController.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
    }
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.EditToLogin, sender: nil)
                }
            })
        }
     }
    
    //MARK: IBAction
    @IBAction func deleteButtonPressed(_ sender: Any) {
        deleteFolderDialog()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if let folderName = textFieldTitle.text {
            editFolderViewModel.renameFolder(folderUUID: self.folderUUID, newName: folderName)
        }
    }
    
    //MARK: DataBinding
    private func setupDataBinding() {
        editFolderViewModel
        .folderDeleted
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { folderDeletedEvent in
            switch folderDeletedEvent {
                case .Success: do {
                    self.dismiss(animated: true, completion: nil)
                }
                case .Error(let error):do {
                    switch error {
                        case .NoUserFound: self.showNoUserFoundDialog()
                        case .UnknownError: self.dialogRepo?.showUnknownErrorDialog(viewController: self)
                    }
                }
            }
        })
        
        editFolderViewModel
        .folderRenamed
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { folderRenameEvent in
            switch folderRenameEvent {
                case .Success: do {
                    self.dismiss(animated: true, completion: nil)
                }
                case .Error(let error): do {
                    if error == .NameTaken {
                        self.dialogRepo?.showNameTakenDialog(viewController: self)
                    } else {
                        self.dialogRepo?.showUnknownErrorDialog(viewController: self)
                    }
                }
            }
        })
    }
    
    //MARK: Dialog
    private func deleteFolderDialog() {
        self.dialogRepo.showDeleteFolderDialog(viewController: self, completion: {
            self.editFolderViewModel?.deleteFolder(folderUUID: self.folderUUID)
        })
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.EditToLogin, sender: nil)
        })
    }
    
    //MARK: TextFieldChangeListener
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.textFieldTitle.text == "" {
            self.saveButton.isEnabled = false
        } else {
            self.saveButton.isEnabled = true
        }
    }
    
}
