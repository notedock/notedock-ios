//
//  ViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 13/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class FoldersViewController
: LoadingSpinnerViewController {
    
    private var folders: [FolderModel] = []
    @IBOutlet var tableView: UITableView!
    
    var foldersViewViewModel: FoldersViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    var cellRepo: CellRepository!
    
    var handle: AuthStateDidChangeListenerHandle?
    
    var folderNameTextField: UITextField!
    
    @IBOutlet var addFolderButton: UIButton!
    var saveAction: UIAlertAction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        foldersViewViewModel.loadFoldersFromFirebase()
        self.showLoading(view: view)
        setupDataBinding()
    }
    
    //MARK: User state
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil || user?.isEmailVerified == false {
                self.performSegue(withIdentifier: CSegue.FoldersToLogin, sender: nil)
            } else {
                user?.reload(completion: { error in
                    if error != nil {
                        self.performSegue(withIdentifier: CSegue.FoldersToLogin, sender: nil)
                    }
                })
            }
        }
    }
       
   override func viewWillDisappear(_ animated: Bool) {
       Auth.auth().removeStateDidChangeListener(handle!)
   }
    
    //MARK: IBActions
    @IBAction func addFolderButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: CDialogTitle.CreateFolder,
                                                message: CDialogMessage.CreateFolder,
                                                preferredStyle: .alert)
        alertController.addTextField { (nameTextField) in
            nameTextField.placeholder = CPlaceholder.NoteBook
            self.folderNameTextField = nameTextField
        }

        let cancelAction = UIAlertAction(title: CAction.Cancel, style: .cancel)

        let saveAction = UIAlertAction(title: CAction.Save, style: .default) { action in
            let folderNameText = self.folderNameTextField.text
            if let folderName = folderNameText {
                self.foldersViewViewModel.createFolder(folderName: folderName)
            }
        }
        //Disabling save action when Folder name is empty
        self.saveAction = saveAction
        self.saveAction.isEnabled = false
        folderNameTextField.addTarget(self, action: #selector(FoldersViewController.textFieldDidChange(_:)),
                                      for: UIControl.Event.editingChanged)

        alertController.addAction(cancelAction)
        alertController.addAction(self.saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: CSegue.FoldersToSettings, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == CSegue.FoldersToFolder {
            let indexPath = sender as! IndexPath
            let folder = self.folders[indexPath.row]
            
            let vc = segue.destination as! FolderViewController
            vc.folder = folder
        } else if segue.identifier == CSegue.FoldersToEdit {
            let indexPath = sender as! IndexPath
            let vc = segue.destination as! EditFolderViewController
            guard let folderUUID = self.folders[indexPath.row].uid else {
                return
            }
            guard let folderName = self.folders[indexPath.row].name else {
                return
            }
            vc.folderName = folderName
            vc.folderUUID = folderUUID
        }
    }
    
    //MARK: Databinding
    private func setupDataBinding() {
        
        foldersViewViewModel
            .loadFolders
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loadFoldersEvent in
                self.dismissLoading()
                switch loadFoldersEvent {
                    case .Success(let folders): do {
                        self.folders = folders
                        self.tableView.reloadData()
                    }
                    case .Error(let error): do {
                        switch error {
                            case .NoUserFound: self.showNoUserFoundDialog()
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
        
        foldersViewViewModel
        .createFolder
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { addFolderEvent in
                switch addFolderEvent {
                case .Error(let error): do {
                    switch error {
                        case .NoUserFound: self.showNoUserFoundDialog()
                        case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        case .NameTaken: self.dialogRepo.showNameTakenDialog(viewController: self)
                    }
                }
                case .Added(let documentID):
                    print("Folder \(documentID) added")
                }
            })
        
        foldersViewViewModel
            .deleteFolder
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { folderDeleted in
                switch folderDeleted {
                    case .Success: do {
                        self.tableView.reloadData()
                    }
                    case .Error(let error):do {
                        switch error {
                            case .NoUserFound: self.showNoUserFoundDialog()
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
        
        foldersViewViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                super.dismissLoading()
            })
    }
    
    //MARK: TextFieldChangeListener
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.folderNameTextField.text == "" {
            self.saveAction.isEnabled = false
        } else {
            self.saveAction.isEnabled = true
        }
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.FoldersToLogin, sender: nil)
        })
    }
    
    private func deleteFolderDialog(indexPath: IndexPath) {
        dialogRepo.showDeleteFolderDialog(viewController: self, completion: {
            if let folderUUID = self.folders[indexPath.row].uid {
                self.foldersViewViewModel.deleteFolder(folderUUID: folderUUID)
            }
        })
    }
}

extension FoldersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.folders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cellRepo.buildFolderCell(indexPath: indexPath)
        cell.bindData(folder: self.folders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: CSegue.FoldersToFolder, sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .default, title: CAction.Edit) { (action, indexPath) in
            self.performSegue(withIdentifier: CSegue.FoldersToEdit, sender: indexPath)
        }
        let delete = UITableViewRowAction(style: .destructive, title: CAction.Delete) { (action ,indexPath) in
            self.deleteFolderDialog(indexPath: indexPath)
        }
        edit.backgroundColor = UIColor.init(named: "NoteDockColor")
        return [delete, edit]
    }
}

