//
//  UpdateEmailViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 21/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class UpdateEmailViewController: LoadingSpinnerViewController {
    @IBOutlet weak var buttonUpdate: UIBarButtonItem!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    
    var updateEmailViewModel: UpdateEmailViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    override func viewDidLoad() {
        
        textFieldsChangeListeners()
        setupDataBinding()
    }
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.UpdateEmailToLogin, sender: nil)
                }
            })
        }
     }
    
    //Disabling Update button when one TextField is empty
    private func textFieldsChangeListeners() {
        textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textFieldPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func setupDataBinding() {
        updateEmailViewModel
            .updateEmail
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { updateEmailEvent in
                switch updateEmailEvent {
                    case .Success: self.showEmailChangedDialog()
                    case .Error(let error):do {
                        switch error {
                            case .UseNewEmail: self.dialogRepo.showUseNewEmailDialog(viewController: self)
                            case .EmailAlreadyUsed: self.dialogRepo.showEmailAlreadyUsedDialog(viewController: self)
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .InvalidPassword: self.dialogRepo.showInvalidPasswordDialog(viewController: self)
                            case .InvalidEmail: self.dialogRepo.showInvalidEmailDialog(viewController: self)
                            case .TryAgainError: self.dialogRepo.showTryAgainDialog(viewController: self)
                            case .NoUserFound: self.showNoUserFoundDialog()
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
        
        updateEmailViewModel
        .showLoading
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { loading in
            if loading {
                super.showLoading(view: self.view)
            } else {
                super.dismissLoading()
            }
        })
    }
    
    //MARK: Dialog
    private func showEmailChangedDialog() {
        self.dialogRepo.showEmailChangedDialog(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.UpdateEmailToLogin, sender: nil)
        })
    }
    
    private func showNoUserFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.UpdateEmailToLogin, sender: nil)
        })
    }
    
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.textFieldEmail.text == "" || self.textFieldPassword.text == "" {
            buttonUpdate.isEnabled = false
        } else {
            buttonUpdate.isEnabled = true
        }
    }
    
    @IBAction func textFieldEmailReturnPressed(_ sender: Any) {
        self.textFieldPassword.becomeFirstResponder()
    }
    
    @IBAction func textFieldPasswordReturnPressed(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func updateEmail(_ sender: Any?) {
        view.endEditing(true)
        guard let email = self.textFieldEmail.text else {
            return
        }
        guard let password = self.textFieldPassword.text else {
            return
        }
        updateEmailViewModel.updateEmail(email: email, password: password)
    }
}
