//
//  NoteViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class NoteViewController: TextFieldViewController {
    
    //Args
    var noteUUID: String!
    var folderUUID: String!
    
    var deletingNote = false
    
    var noteViewViewModel: NoteViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    @IBOutlet var textViewTitle: UITextView!
    @IBOutlet var textViewDescription: UITextView!
    
    override func viewDidLoad() {
        noteViewViewModel.loadNote(folderUUID: folderUUID, noteUUID: noteUUID)
        setupDataBinding()
    }
    
    //MARK: User state
     override func viewWillAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: { error in
                if error != nil {
                    self.performSegue(withIdentifier: CSegue.NoteToLogin, sender: nil)
                }
            })
        }
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        //If user is just exiting view and not deleting note, save note
        if(!deletingNote) {
            saveNote()
        }
    }
    
    //MARK: IBAction
    @IBAction func deleteNoteButtonPressed(_ sender: Any) {
        self.dialogRepo.showDeleteNoteDialog(viewController: self, completion: {
            self.noteViewViewModel.deleteNote(folderUUID: self.folderUUID, noteUUID: self.noteUUID)
        })
    }
    
    //MARK: DataBinding
    private func setupDataBinding() {
        noteViewViewModel
            .noteLoaded
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {loadNoteEvent in
                switch loadNoteEvent {
                    case .Success(let note): do {
                            self.textViewTitle.text = note.title
                            self.textViewDescription.text = note.description
                        }
                    case .Error(let error): do {
                        switch error {
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                            case .NoUserFound: self.showUserNotFoundDialog()
                        }
                    }
                }
            })
        
        noteViewViewModel
            .noteDeleted
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { deleteNoteEvent in
                switch deleteNoteEvent {
                    case .Success: do {
                        self.deletingNote = true
                        self.performSegue(withIdentifier: CSegue.NoteToFolder, sender: self)
                    }
                    case .Error(let error): do {
                        switch error {
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                            case .NoUserFound: self.showUserNotFoundDialog()
                        }
                    }
                }
            })
        
        noteViewViewModel
            .noteSaved
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { saveNoteEvent in
                switch saveNoteEvent {
                    case .Success: do {
                        self.performSegue(withIdentifier: CSegue.NoteToFolder, sender: self)
                    }
                    case .Error(let error): do {
                        switch error {
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                            case .NoUserFound: self.showUserNotFoundDialog()
                        }
                    }
                }
            })
    }
    
    //MARK: Dialog
    private func showUserNotFoundDialog() {
        self.dialogRepo.showNoUserFound(viewController: self, completion: {
            self.performSegue(withIdentifier: CSegue.NoteToLogin, sender: nil)
        })
    }
    
    private func saveNote() {
        guard let title = textViewTitle.text else {
            print("Title is nil")
            return
        }
        guard let description = textViewDescription.text else {
            print("Description is nil")
            return
        }
        let note = NoteModel(uid: noteUUID, title: title, description: description)
        noteViewViewModel.updateNote(folderUUID: folderUUID, note: note)
    }
}
