//
//  LoginViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 29/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class LoginViewController: LoadingSpinnerViewController {
    
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    @IBOutlet var labelNoAccount: UILabel!
    @IBOutlet var labelTroublesLoggin: UILabel!
    
    var loginViewModel: LoginViewViewModelProtocol!
    var dialogRepo: DialogRepository!
    
    @IBOutlet var buttonLogin: MainButton!
    
    override func viewDidLoad() {
        setupDataBinding()
        
        //Disabling Login button when one TextField is empty
        textFieldPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        //Label tap recognizer
        let noAccountTap = UITapGestureRecognizer(target: self, action: #selector(noAccountLabelClicked(sender:)))
        labelNoAccount.addGestureRecognizer(noAccountTap)
        let troublesLogginTap = UITapGestureRecognizer(target: self, action: #selector(troublesLogginLabelClicked(sender:)))
        labelTroublesLoggin.addGestureRecognizer(troublesLogginTap)
    }
    
    //MARK: IBAction
    @IBAction func loginButtonPressed(_ sender: Any) {
        let emailText = textFieldEmail.text
        let passwordText = textFieldPassword.text
        
        guard let email = emailText else {
            print("Email is nil")
            return
        }
        guard let password = passwordText else {
            print("Password is nil")
            return
        }
        view.endEditing(true)
        loginViewModel.login(email: email, password: password)
    }
    
    //MARK: Databinding
    func setupDataBinding() {
        loginViewModel
        .login
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { loginEvent in
            switch loginEvent {
                case .Success: do {
                        self.performSegue(withIdentifier: CSegue.LoginToNavigation, sender: nil)
                    }
                case .Error(let loginError): do {
                    switch loginError {
                        case .InvalidEmail: self.dialogRepo.showInvalidEmailDialog(viewController: self)
                        case .BadCredentials: self.dialogRepo.showBadCredentialsDialog(viewController: self)
                        case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                        case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        case .UserNotVerified: self.userNotVerifiedDialog()
                    }
                }
            }
        })
        
        loginViewModel
            .showLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loading in
                if loading {
                    self.buttonLogin.isEnabled = false
                    super.showLoading(view: self.view)
                } else {
                    self.buttonLogin.isEnabled = true
                    super.dismissLoading()
                }
            })
        
        loginViewModel
            .sendVerificationEmail
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { emailEvent in
                self.buttonLogin.isEnabled = true
                switch emailEvent {
                case .Success: self.dialogRepo.showVerificationEmailSent(viewController: self)
                    case .Error(let emailError): do {
                        switch emailError {
                            case .NetworkError: self.dialogRepo.showNetworkErrorDialog(viewController: self)
                            case .TooManyRequests: self.dialogRepo.showTooManyRequestsDialog(viewController: self)
                            case .UnknownError: self.dialogRepo.showUnknownErrorDialog(viewController: self)
                        }
                    }
                }
            })
    }
    
    //MARK: Dialog
    private func userNotVerifiedDialog() {
        self.dialogRepo.showUserNotVerifiedDialog(viewController: self, completion: {
            if let user = Auth.auth().currentUser {
                self.loginViewModel.sendVerificationEmail(user: user)
            }
        })
    }
    
    //MARK: TextFieldChangeListener
    @objc func textFieldDidChange(_ action: UIAlertAction) {
        if self.textFieldEmail.text == "" || self.textFieldPassword.text == "" {
            self.buttonLogin.isEnabled = false
        } else {
            self.buttonLogin.isEnabled = true
        }
    }
    
    //MARK: Label Click
    @objc func noAccountLabelClicked(sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: CSegue.LoginToRegister, sender: nil)
    }
    
    @objc func troublesLogginLabelClicked(sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: CSegue.LoginToResetSeg, sender: nil)
    }
    
    //Unwind from Cancel button in ForgotPassword
    @IBAction func unwindToLogin(_ unwindSegue: UIStoryboardSegue) {
        _ = unwindSegue.source
    }
}
