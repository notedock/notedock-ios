//
//  SwinjectStoryboard.swift
//  NoteDock
//
//  Created by Branislav Bily on 13/09/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {
    class func setup() {
        
        //MARK: ViewController
        //Folders
        defaultContainer.storyboardInitCompleted(FoldersViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.cellRepo = r.resolve(CellRepository.self)!
            c.foldersViewViewModel = r.resolve(FoldersViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(FolderViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.cellRepo = r.resolve(CellRepository.self)!
            c.folderViewViewModel = r.resolve(FolderViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(EditFolderViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.editFolderViewModel = r.resolve(EditFolderViewViewModelProtocol.self)!
        }
        //Authentication
        defaultContainer.storyboardInitCompleted(LoginViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.loginViewModel = r.resolve(LoginViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(RegisterViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.registerViewModel = r.resolve(RegisterViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(ResetPasswordViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.resetPasswordViewModel = r.resolve(ResetPasswordViewViewModelProtocol.self)!
        }
        //Note
        defaultContainer.storyboardInitCompleted(NoteViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.noteViewViewModel = r.resolve(NoteViewViewModelProtocol.self)!
        }
        //Settings
        defaultContainer.storyboardInitCompleted(SettingsViewController.self) { r, c in
            c.cellRepo = r.resolve(CellRepository.self)!
            c.settingsViewModel = r.resolve(SettingsViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(AccountViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.cellRepo = r.resolve(CellRepository.self)!
            c.accountViewModel = r.resolve(AccountViewViewModelProcotol.self)!
        }
        defaultContainer.storyboardInitCompleted(ChangePasswordViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.changePasswordViewModel = r.resolve(ChangePasswordViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(HelpAndSupportViewController.self) { r, c in
            c.cellRepo = r.resolve(CellRepository.self)!
            c.helpAndSupportViewModel = r.resolve(HelpAndSupportViewViewModelProtocol.self)!
        }
        defaultContainer.storyboardInitCompleted(UpdateEmailViewController.self) { r, c in
            c.dialogRepo = r.resolve(DialogRepository.self)!
            c.updateEmailViewModel = r.resolve(UpdateEmailViewModelProtocol.self)!
        }
        

        //MARK: UseCase
        //Folder
        defaultContainer.register(FoldersFromFirebaseUseCase.self) { _ in FoldersFromFirebaseImpl()}
        defaultContainer.register(AddFolderToFirebaseUseCase.self) { _ in AddFolderToFirebaseImpl()}
        defaultContainer.register(FolderNameTakenUseCase.self) { _ in FolderNameTakenImpl()}
        defaultContainer.register(DeleteFolderFromFirebaseUseCase.self) { _ in DeleteFolderFromFirebaseImpl()}
        defaultContainer.register(RenameFolderUsecase.self) { r in RenameFolderImpl()}

        //Note
        defaultContainer.register(LoadNotesFromFirebaseUseCase.self)   { _ in LoadNotesFromFirebaseImpl()}
        defaultContainer.register(LoadNoteFromFirebaseUseCase.self) { _ in LoadNoteFromFirebaseImpl()}
        defaultContainer.register(AddNoteToFirebaseUseCase.self) { _ in AddNoteToFirebaseImpl()}
        defaultContainer.register(DeleteNoteFromFirebaseUseCase.self) { _ in DeleteNoteFromFirebaseImpl()}
        defaultContainer.register(UpdateNoteToFirebaseUsecase.self) { _ in UpdateNoteToFirebaseImpl()}
        
        //Authentication
        defaultContainer.register(EmailValidatorUseCase.self) { _ in EmailValidatorImpl()}
        
        //MARK: Repository
        defaultContainer.register(AuthRepository.self) { _ in AuthRepositoryImpl()}
        defaultContainer.register(DialogRepository.self) { _ in DialogRepositoryImpl()}
        defaultContainer.register(CellRepository.self) { _ in CellRepositoryImpl()}
        
        
        //MARK: ViewModel
        //Folder
        defaultContainer.register(FoldersViewViewModelProtocol.self) { r in
            let viewModel = FoldersViewViewModel()
            viewModel.foldersFromFirebase = r.resolve(FoldersFromFirebaseUseCase.self)!
            viewModel.addFolderToFirebase = r.resolve(AddFolderToFirebaseUseCase.self)!
            viewModel.folderNameTaken = r.resolve(FolderNameTakenUseCase.self)!
            viewModel.deleteFolderFromFirebase = r.resolve(DeleteFolderFromFirebaseUseCase.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(FolderViewViewModelProtocol.self) { r in
            let viewModel = FolderViewViewModel()
            viewModel.loadNotesFromFirebase = r.resolve(LoadNotesFromFirebaseUseCase.self)!
            viewModel.addNoteToFirebase = r.resolve(AddNoteToFirebaseUseCase.self)!
            viewModel.deleteNoteFromFirebase = r.resolve(DeleteNoteFromFirebaseUseCase.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(EditFolderViewViewModelProtocol.self) { r in
            let viewModel = EditFolderViewViewModel()
            viewModel.deleteFolder = r.resolve(DeleteFolderFromFirebaseUseCase.self)!
            viewModel.renameFolder = r.resolve(RenameFolderUsecase.self)!
            viewModel.folderNameTaken = r.resolve(FolderNameTakenUseCase.self)!
            return viewModel
        }.inObjectScope(.container)
    
        //Note
        defaultContainer.register(NoteViewViewModelProtocol.self) { r in
            let viewModel = NoteViewViewModel()
            viewModel.loadNoteFromFirebase = r.resolve(LoadNoteFromFirebaseUseCase.self)!
            viewModel.deleteNoteFromFirebase = r.resolve(DeleteNoteFromFirebaseUseCase.self)!
            viewModel.updateNoteFromFirebase = r.resolve(UpdateNoteToFirebaseUsecase.self)!
            return viewModel
        }.inObjectScope(.container)
        
        //Authentication
        defaultContainer.register(LoginViewViewModelProtocol.self) { r in
            let viewModel = LoginViewViewModel()
            viewModel.emailValidator = r.resolve(EmailValidatorUseCase.self)!
            viewModel.authRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(RegisterViewViewModelProtocol.self) { r in
            let viewModel = RegisterViewViewModel()
            viewModel.emailValidator = r.resolve(EmailValidatorUseCase.self)!
            viewModel.authRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(ResetPasswordViewViewModelProtocol.self) { r in
            let viewModel = ResetPasswordViewViewModel()
            viewModel.emailValidator = r.resolve(EmailValidatorUseCase.self)!
            viewModel.authRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
        
        //Settings
        defaultContainer.register(SettingsViewViewModelProtocol.self) { r in
            let viewModel = SettingsViewViewModel()
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(AccountViewViewModelProcotol.self) { r in
            let viewModel = AccountViewViewModel()
            viewModel.authenticationRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(ChangePasswordViewViewModelProtocol.self) { r in
            let viewModel = ChangePasswordViewViewModel()
            viewModel.authRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(HelpAndSupportViewViewModelProtocol.self) { r in
            let viewModel = HelpAndSupportViewViewModel()
            return viewModel
        }.inObjectScope(.container)
        
        defaultContainer.register(UpdateEmailViewModelProtocol.self) { r in
            let viewModel = UpdateEmailViewModel()
            viewModel.emailValidator = r.resolve(EmailValidatorUseCase.self)!
            viewModel.authRepository = r.resolve(AuthRepository.self)!
            return viewModel
        }.inObjectScope(.container)
    }
}
