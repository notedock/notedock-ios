//
//  SettingsActionCellViewController.swift
//  NoteDock
//
//  Created by Branislav Bily on 31/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class SettingsActionCellViewController: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    
    func bindData(title: String) {
        labelTitle.text = title
    }
}
