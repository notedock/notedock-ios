//
//  LoginRegisterButton.swift
//  NoteDock
//
//  Created by Branislav Bily on 30/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import UIKit

class MainButton: UIButton {
    override public var isEnabled: Bool {
        didSet {
            if self.isEnabled {
                self.backgroundColor = self.backgroundColor?.withAlphaComponent(1.0)
                
            } else {
                self.backgroundColor = self.backgroundColor?.withAlphaComponent(0.3)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 10.0
        self.contentEdgeInsets.bottom = 1
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.gray, for: .disabled)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        self.backgroundColor = UIColor.init(named: "NoteDockColor")
        self.isEnabled = false
    }
    
    

}
