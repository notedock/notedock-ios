//
//  CellRepository.swift
//  NoteDock
//
//  Created by Branislav Bily on 25/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//

import UIKit

protocol CellRepository {
    func buildFolderCell(indexPath: IndexPath) -> FolderCellViewController
    func buildNoteCell(indexPath: IndexPath) -> NoteCellViewController
    func buildSettingsCell(indexPath: IndexPath) -> SettingsCellViewController
    func buildAccountDeleteCell(indexPath: IndexPath) -> AccountDeleteCellViewController
    func buildAccountDoubleDetailCell(indexPath: IndexPath) -> AccountDoubleDetailCellViewController
    func buildAccountDetailCell(indexPath: IndexPath) -> AccountDetailCellViewController
    func buildSettingsActionCell(indexPath: IndexPath) -> SettingsActionCellViewController
}

class CellRepositoryImpl: CellRepository {
    
    let tableView: UITableView = UITableView()
    
    init() {
        self.tableView.register(UINib(nibName: "FolderCell", bundle: nil), forCellReuseIdentifier: "FolderCell")
        self.tableView.register(UINib(nibName: "NoteCell", bundle: nil), forCellReuseIdentifier: "NoteCell")
        self.tableView.register(UINib(nibName: "SettingsCell", bundle: nil), forCellReuseIdentifier: "SettingsCell")
        self.tableView.register(UINib(nibName: "SettingsActionCell", bundle: nil), forCellReuseIdentifier: "SettingsActionCell")
        self.tableView.register(UINib(nibName: "AccountDoubleDetailCell", bundle: nil), forCellReuseIdentifier: "AccountDoubleDetailCell")
        self.tableView.register(UINib(nibName: "AccountDeleteCell", bundle: nil), forCellReuseIdentifier: "AccountDeleteCell")
        self.tableView.register(UINib(nibName: "AccountDetailCell", bundle: nil), forCellReuseIdentifier: "AccountDetailCell")
        
    }
    
    func buildFolderCell(indexPath: IndexPath) -> FolderCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath) as! FolderCellViewController
    }
    
    func buildNoteCell(indexPath: IndexPath) -> NoteCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath) as! NoteCellViewController
    }
    
    func buildSettingsCell(indexPath: IndexPath) -> SettingsCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCellViewController
    }
    
    func buildAccountDeleteCell(indexPath: IndexPath) -> AccountDeleteCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "AccountDeleteCell") as! AccountDeleteCellViewController
    }
    
    func buildAccountDoubleDetailCell(indexPath: IndexPath) -> AccountDoubleDetailCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "AccountDoubleDetailCell") as! AccountDoubleDetailCellViewController
    }
    
    func buildAccountDetailCell(indexPath: IndexPath) -> AccountDetailCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "AccountDetailCell", for: indexPath) as! AccountDetailCellViewController
    }
    
    func buildSettingsActionCell(indexPath: IndexPath) -> SettingsActionCellViewController {
        return self.tableView.dequeueReusableCell(withIdentifier: "SettingsActionCell", for: indexPath) as! SettingsActionCellViewController
    }
    
}
