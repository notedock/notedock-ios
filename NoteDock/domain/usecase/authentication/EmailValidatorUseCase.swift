//
//  EmailValidator.swift
//  NoteDock
//
//  Created by Branislav Bily on 23/01/2020.
//  Copyright © 2020 Pixel Art. All rights reserved.
//
import Foundation

protocol EmailValidatorUseCase {
    func validateEmail(email: String) -> Bool
}

class EmailValidatorImpl: EmailValidatorUseCase {
    func validateEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
}
