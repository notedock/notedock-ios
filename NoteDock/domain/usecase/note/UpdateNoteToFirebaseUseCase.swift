//
//  UpdateNoteToFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebase
import FirebaseFirestore

protocol UpdateNoteToFirebaseUsecase {
    func updateNote(folderUUID: String, note: NoteModel, user: User) -> Completable
}

class UpdateNoteToFirebaseImpl: UpdateNoteToFirebaseUsecase {
    func updateNote(folderUUID: String, note: NoteModel, user: User) -> Completable {
    return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .document(folderUUID)
                .collection(CCollections.Notes)
                .document(note.uid!)
                .rx
                .updateData([CNote.Title: note.title!,
                             CNote.Description: note.description!,
                             CNote.Updated: Timestamp(date: Date())])
                .asSingle()
                .asCompletable()
    }
}



