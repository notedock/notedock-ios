//
//  LoadNoteFromFirebaseUseCase.swift
//  NoteDock
//
//  Created by Branislav Bily on 28/12/2019.
//  Copyright © 2019 Pixel Art. All rights reserved.
//

import RxSwift
import FirebaseAuth
import RxFirebaseFirestore

protocol LoadNoteFromFirebaseUseCase{
    func loadNote(folderUUID: String, noteUUID: String, user: User) -> Single<NoteModel>
}

class LoadNoteFromFirebaseImpl: LoadNoteFromFirebaseUseCase {
    func loadNote(folderUUID: String, noteUUID: String, user: User) -> Single<NoteModel> {
        return firestoreReference.collection(CCollections.Users)
                .document(user.uid)
                .collection(CCollections.Folders)
                .document(folderUUID)
                .collection(CCollections.Notes)
                .document(noteUUID)
                .rx
                .getDocument()
                .map{ document -> NoteModel in
                    return NoteModel(document: document)
                }
                .asSingle()
    }
}
